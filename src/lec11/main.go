// Lecture 11 - Receivers: Structs with functions
package main

import "log"

type myStruct struct {
	firstName string
	lastName string
	age int
	job string
}

func (h *myStruct) saySomething() string {
	return h.lastName
}

func (a *myStruct) getAgeInfo() int {
	return a.age
}

func main() {

	var myVar myStruct
	myVar.firstName = "Chris"
	myVar.lastName = "Nguyen"
	myVar.age = 49

	myVar2 := myStruct {
		firstName: "Mike",
		lastName: "McNillin",
		age: 30,
		job: "Tennis Coach",
	}

	log.Println("myVar is set to", myVar.firstName)
	log.Println("myVar2 is set to", myVar2.firstName)
	log.Println("myVar say", myVar.saySomething())
	log.Println("myVar2 say", myVar2.saySomething())
	log.Println("myVar age is", myVar.getAgeInfo())
	log.Println("myVar2 age is", myVar2.getAgeInfo())
}